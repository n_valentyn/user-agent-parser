<?php
require_once '../vendor/autoload.php';

//$UAParser =  new \AdditionComponentsNVM\UAParser\WhichBrowserAdapter();

$UAParser =  new \AdditionComponentsNVM\UAParser\UAParserAdapter();

echo '<pre>' . PHP_EOL;

echo $UAParser->parse('Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0') . PHP_EOL;

echo $UAParser->getDevice() . PHP_EOL;

echo $UAParser->getOs() . PHP_EOL;

echo $UAParser->getBrowser() . PHP_EOL;

echo $UAParser->getEngine() . PHP_EOL;
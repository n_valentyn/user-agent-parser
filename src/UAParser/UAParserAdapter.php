<?php


namespace AdditionComponentsNVM\UAParser;


class UAParserAdapter implements AdapterInterface
{
    protected $userAgentInfo;

    public function parse(string $userAgent)
    {
        $uaParser = new \UAParser\UAParser();
        $this->userAgentInfo = $uaParser->parse($userAgent);
    }

    public function getBrowser()
    {
        return $this->userAgentInfo->getBrowser()->getFamily();
    }

    public function getEngine()
    {
        return $this->userAgentInfo->getRenderingEngine()->getFamily();
    }

    public function getOs()
    {
        return $this->userAgentInfo->getOperatingSystem()->getFamily();
    }

    public function getDevice()
    {
        return $this->userAgentInfo->getDevice()->getType();
    }
}

<?php


namespace AdditionComponentsNVM\UAParser;


class WhichBrowserAdapter implements AdapterInterface
{
    protected $userAgentInfo;

    public function parse(string $userAgent)
    {
        $this->userAgentInfo = new \WhichBrowser\Parser($userAgent);
    }

    public function getBrowser()
    {
        return $this->userAgentInfo->browser->name;
    }

    public function getEngine()
    {
        return $this->userAgentInfo->engine->name;
    }

    public function getOs()
    {
        return $this->userAgentInfo->os->name;
    }

    public function getDevice()
    {
        return $this->userAgentInfo->device->type;
    }
}

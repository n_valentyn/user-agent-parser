<?php


namespace AdditionComponentsNVM\UAParser;


interface AdapterInterface
{
    public function parse(string $userAgent);

    public function getBrowser();

    public function getEngine();

    public function getOs();

    public function getDevice();
}
